---
layout: layout.njk
---

# 📝 Les GitLab Pages avec GitLab CI
> Dans la série "🚀 Micro Hands-on"

Date du workshop: [Jour-Mois-Année]

> **Avertissement**: ce workshop est complètement expérimental

## Qu'est-ce qu'un "Micro Hands-on" ?

Un "micro hands-on" est un workshop préparé de manière à pouvoir être dispensé
- en 1 heure
- en remote (fonctionne aussi en présentiel)
- en ayant le moins de choses possible à installer (si possible rien)

Le "micro hands-on" peut ensuite être facilement rejoué seul

## Détails / Contenu

- Vous allez apprendre à publier des pages statiques simplement et facilement avec les **GitLab Pages** et **GitLab CI/CD**
- Il y aura 3 exercices pour vous donner un bon aperçu de ce que vous pouvez faire

## Prérequis

- Un navigateur (récent)
- Un compte sur **[GitLab.com](https://gitlab.com/)**
- Un compte sur **[Twitch](https://www.twitch.tv)**

## Comment s'inscrire à ce workshop

🤔 **Mais pourquoi s'inscrire ?**: Je vais vous créer des environnements de travail à l'avance (un groupe et des projets par participants)

👋 **Vous devez vous inscrire avant le [Jour-Mois-Année]**

🖐️ **Attention**: seuls les N premiers inscrits seront enregistrés pour cette session.

- Allez sur le projet **[registrations]()**
- Créer une issue dans le projet (juste un titre pour dire bonjour)
- La veille du workshop vous aurez accès au projet **[communications]()** avec tous les détails pour vous connecter (certainement sur [https://www.twitch.tv/k33g_org](https://www.twitch.tv/k33g_org))

à bientôt 😃
